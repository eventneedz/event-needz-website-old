//import Header from './components/Header';
import Navigation from './components/Common/Navigation';
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './components/Home/Home';
import UseEffectAPI from './components/User/User';
import DatatablePage from './components/User/DatatablePage ';
import AddUser from './components/User/AddUser';
import VerifyProduct from './components/Verify/VerifyProduct';
import VerifyUser from './components/Verify/VerifyUser';
import AddVerify from './components/Verify/AddVerify';
import Category from './components/Category/Category';
import CategoryView from './components/Category/CategoryView';
import AddCategory from './components/Category/AddCategory';
import UpdateCategory from './components/Category/UpdateCategory';
import Product from './components/Product/Product';
import UpdateProduct from './components/Product/UpdateProduct';
import AddProduct from './components/Product/ProductAdd';
import ProductView from './components/Product/ProductView';
import PaymentManagement from './components/Payment/Payment_Management';
import Report from './components/Report/Reports';
import Vendors from './components/Vendors/Vendors';
import AddVendor from './components/Vendors/AddVendor';
import Header from './components/Common/Header';
import Leads from './components/Enquery/Leads';
import Enquery from './components/Enquery/Enquery';
import EnqueryView from './components/Enquery/EnqueryView';
import SendEnquery from './components/Enquery/SendEnquery';
import Login from './components/Login';
// import Logout from './components/Logout';
import Profilenew from './components/Profilenew';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import Footer from './components/Common/Footer';
//import Accordian from './components/Accordian/Accordian';
import './Style.css';
import CovidData from './components/ApiTest/Covid_data';
import DataTest from './components/ApiTest/DataTest';
import { fetchData } from './services/Api';
let session_id = sessionStorage.getItem("usrses");
// alert(session_id);
class App extends React.Component {
  render() {

    if (session_id != null) {

      return (

        <Router>
          {/* <Routes>
            <Route exact path='/' element={< Login />}></Route>

          </Routes> */}

          <div className="App">

            <Header />
            <Footer />
            <Navigation />

            <div className="content-wrapper">
              <div className="container-full">

                {/* <Accordian />*/}
                <Routes>
                  <Route exact path='/Das hboard' element={< Home />}></Route>
                  
                  <Route exact path='/User' element={< UseEffectAPI />}></Route>
                  <Route exact path='/Product' element={< Product />}></Route>
                  <Route exact path='/UpdateProduct/:id' element={< UpdateProduct/>}></Route>
                  <Route exact path='/AddProduct' element={< AddProduct />}></Route>
                  <Route exact path='/AddUser' element={< AddUser />}></Route>
                  <Route exact path='/Category' element={< Category />}></Route>
                  <Route exact path='/AddCategory' element={< AddCategory />}></Route>
                  <Route exact path='/CategoryView' element={< CategoryView />}></Route>
                  <Route exact path='/UpdateCategory/:id' element={< UpdateCategory/>}></Route> 
                  <Route  path='/CategoryView/:id' element={< CategoryView />}></Route>
                  <Route exact path='/PaymentManagement' element={< PaymentManagement />}></Route>
                  <Route exact path='/Report' element={< Report />}></Route>
                  <Route exact path='/Vendors' element={< Vendors />}></Route>
                  <Route exact path='/AddVendor' element={< AddVendor />}></Route>
                  <Route exact path='/ProductView' element={< ProductView />}></Route>
                  <Route  path='/ProductView/:id' element={< ProductView />}></Route> 
                  <Route exact path='/VerifyProduct' element={< VerifyProduct />}></Route>
                  <Route exact path='/AddVerify' element={< AddVerify />}></Route>
                  <Route exact path='/VerifyUser' element={< VerifyUser />}></Route>
                  <Route exact path='/DataTest' element={< DataTest />}></Route>
                  <Route exact path='/Enquery' element={< Enquery />}></Route>
                  <Route exact path='/EnqueryView' element={< EnqueryView />}></Route>
                  <Route exact path='/SendEnquery' element={< SendEnquery />}></Route>
                  <Route exact path='/Leads' element={< Leads />}></Route>
                  <Route exact path='/DatatablePage' element={< DatatablePage />}></Route>
                  {/* <Route exact path='/Logout' element={< Logout />}></Route> */}

                </Routes>

              </div>
            </div>
          </div>
        </Router>
      );
    } else {
   
      return (
        <>
       

       < Router>
        <Routes>
          
          <Route exact path='/' element={< Login />}></Route>

        </Routes>
        </Router>
</>

      );

    }
  }
}
export default App;
