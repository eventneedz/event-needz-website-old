import React from "react";
import { MDBContainer } from "mdbreact";
import { Line } from "react-chartjs-2";
  
const Chart = () => {
  
  // Sample data
  const data = {
    labels: ["Sunday", "Monday", "Tuesday",
      "Wednesday", "Thursday", "Friday", "Saturday"],
    datasets: [
      {
        label: "Hours Studied in Geeksforgeeks",
        data: [2, 5, 7, 9, 7, 6, 4],
        fill: true,
        backgroundColor: "rgba(6, 156,51, .3)",
        borderColor: "#02b844",
      }
    ]
  }
  
  return (
    <MDBContainer>
      <Line data={data} />
    </MDBContainer>
  );
}
  
export default Chart;