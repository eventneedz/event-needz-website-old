//import Header from './components/Header';
import Navigation from './components/Common/Navigation';
import React from 'react';
import { BrowserRouter as Router,Routes, Route } from 'react-router-dom';
import Home from './components/Home/Home';
import User from './components/User/User';
import AddUser from './components/User/AddUser';
import VerifyProduct from './components/Verify/VerifyProduct';
import VerifyUser from './components/Verify/VerifyUser';
import AddVerify from './components/Verify/AddVerify';
import Category from './components/Category/Category';
import CategoryView from './components/Category/CategoryView';
import AddCategory from './components/Category/AddCategory';
import Product from './components/Product/Product';
import AddProduct from './components/Product/ProductAdd';
import ProductView from './components/Product/ProductView';
import PaymentManagement from './components/Payment/Payment_Management';
import Report from './components/Report/Reports';
import Vendors from './components/Vendors/Vendors';
import Header from './components/Common/Header';
import '../node_modules/font-awesome/css/font-awesome.min.css'; 
import Footer from './components/Common/Footer';
//import Accordian from './components/Accordian/Accordian';
import './Style.css';
import CovidData from './components/ApiTest/Covid_data';
import { fetchData } from './services/Api';
class App extends React.Component{
  constructor(){
    super();
    this.state={
      data:{},
      
  
    }
  }
        async componentDidMount(){
          const fetchedData =  await fetchData();
          this.setState({
            data:fetchedData
          })
          console.log(fetchedData)
        }
        render(){
          const {data} =  this.state
return(
  
    <Router>
    <div className="App">
    
    
   
    <Header/>
    <Footer />
    <Navigation />
    
    <div className="content-wrapper">
       <div className="container-full">
       <CovidData data={data}  />
      {/* <Accordian />*/}
          <Routes>
                <Route exact path='/' element={< Home />}></Route>
                <Route exact path='/User' element={< User />}></Route>
                <Route exact path='/Product' element={< Product />}></Route>
                <Route exact path='/AddProduct' element={< AddProduct />}></Route>
                <Route exact path='/AddUser' element={< AddUser />}></Route>
                <Route exact path='/Category' element={< Category />}></Route>
                <Route exact path='/AddCategory' element={< AddCategory />}></Route>
                <Route exact path='/CategoryView' element={< CategoryView />}></Route>
                <Route exact path='/PaymentManagement' element={< PaymentManagement />}></Route>
                <Route exact path='/Report' element={< Report />}></Route>
                <Route exact path='/Vendors' element={< Vendors />}></Route>
                <Route exact path='/ProductView' element={< ProductView />}></Route>
                <Route exact path='/VerifyProduct' element={< VerifyProduct />}></Route>
                <Route exact path='/AddVerify' element={< AddVerify />}></Route>
                <Route exact path='/VerifyUser' element={< VerifyUser />}></Route>

             
                
          </Routes>
         
        </div>
    </div>
   </div>
</Router>
  );
}
}
export default App;
