import React from 'react';
import ReactDOM from 'react-dom';


// import './Css/Style.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
//import 'bootstrap/dist/css/bootstrap.min.css';

// Create the function
export function AddLibrary(urlOfTheLibrary) {
  const script = document.createElement('script');
  script.src = urlOfTheLibrary;
  script.async = true;
  document.body.appendChild(script);
}
  
ReactDOM.render(
  <React.StrictMode>
    <App />
    {AddLibrary('js/vendors.min.js')}
    {AddLibrary('js/pages/chat-popup.js')}
    
    {AddLibrary('../assets/icons/feather-icons/feather.min.js')}
    {AddLibrary('../assets/vendor_components/moment/min/moment.min.js')}
    {AddLibrary('../assets/vendor_components/fullcalendar/fullcalendar.js')}
    {AddLibrary('../assets/vendor_components/apexcharts-bundle/dist/apexcharts.js')}
    {AddLibrary('js/template.js')}
    {AddLibrary('js/pages/dashboard2.js')}
    {AddLibrary('js/pages/ecommerce_details.js')}
    {AddLibrary('js/pages/calendar.js')}
    {AddLibrary('js/pages/advanced-form-element.js')}
 
    
  </React.StrictMode>,
  document.getElementById('root')
  
);


reportWebVitals();
