import React from 'react';
import {Link } from "react-router-dom";
//import { Navbar ,Nav } from 'react-bootstrap';


class Navigation extends React.Component{
  

    render(){
      return(
        <aside className="main-sidebar">
        <section className="sidebar position-relative">	
              <div className="multinav">
              <div className="multinav-scroll ps">	
                  <ul className="sidebar-menu tree" data-widget="tree">	
                   
                    <li className="active">
                        <Link to="/Dashboard"><i className="icon-Layout-4-blocks"><span className="path1"></span><span className="path2"></span></i>
                          <span>Dashboard</span></Link>
                    </li>
                    <li className="treeview">
                      <Link to="/User"><i className="icon-Layout-4-blocks"><span className="path1"></span><span className="path2"></span></i>
                          <span>User</span>
                          <span className="pull-right-container">
                                <i className="fa fa-angle-right pull-right"></i>
                            </span>
                          </Link>
                          <ul className="treeview-menu">
                            <li><Link to="/User"><i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>User list</Link></li>
                            <li><Link to="/AddUser"><i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Add User</Link></li>
                        </ul>
                      </li>
                      <li className="treeview">

                      <Link to="/Product">
                            <i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Product
                            <span className="pull-right-container">
                                <i className="fa fa-angle-right pull-right"></i>
                            </span>
                            </Link>
                        <ul className="treeview-menu">
                            <li><Link to="/Product"><i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Product list</Link></li>
                            <li><Link to="/AddProduct"><i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Add Product</Link></li>
                        </ul>
                    </li>
                      
                    <li className="treeview">
                        <Link to="/Category">
                            <i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Category
                            <span className="pull-right-container">
                                <i className="fa fa-angle-right pull-right"></i>
                            </span>
                        </Link>
                        <ul className="treeview-menu">
                            <li><Link to="/Category"><i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Category list</Link></li>
                            <li><Link to="/AddCategory"><i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Add Category</Link></li>
                        </ul>
                    </li>
                    <li className="treeview">
                        <Link to="/Enquery">
                            <i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Enquiry
                            <span className="pull-right-container">
                                <i className="fa fa-angle-right pull-right"></i>
                            </span>
                        </Link>
                        <ul className="treeview-menu">
                            <li><Link to="/Enquery"><i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Enquiry list</Link></li>
                            <li><Link to="/Leads"><i className="icon-Commit"><span className="path1"></span><span className="path2"></span></i>Leads</Link></li>
                        </ul>
                    </li>
                      <li>
                        <Link to="/PaymentManagement">
                          <i className="icon-Layout-4-blocks"><span className="path1"></span><span className="path2"></span></i>
                          <span>Payment management
                        </span>
                        </Link>
                      </li>
                      <li>
                        <Link to="/Report">
                          <i className="icon-Layout-4-blocks"><span className="path1"></span><span className="path2"></span></i>
                          <span>Reports</span>
                        </Link>
                      </li>
                      <li>
                        <Link to="/Vendors">
                          <i className="icon-Layout-4-blocks"><span className="path1"></span><span className="path2"></span></i>
                          <span>vendors </span>
                        </Link>
                      </li>
                      <li>
                        <Link to="/VerifyProduct">
                          <i className="icon-Layout-4-blocks"><span className="path1"></span><span className="path2"></span></i>
                          <span>Verify products</span>
                        </Link>
                      </li>
                      <li>
                        <Link to="/VerifyUser">
                          <i className="icon-Layout-4-blocks"><span className="path1"></span><span className="path2"></span></i>
                          <span>Verify use/vendor</span>
                        </Link>
                      </li>
                      <li>
                        <a href='/' data-toggle="control-sidebar">
                          <i className="icon-Layout-4-blocks"><span className="path1"></span><span className="path2"></span></i>
                          <span>Notiﬁcation</span>
                        </a>
                      </li>
                      
                                  
                  </ul>
              <div className="ps__rail-x"><div className="ps__thumb-x" ></div></div><div className="ps__rail-y" ><div className="ps__thumb-y"></div></div></div>
            </div>
        </section>
     { /* <div className="sidebar-footer">
            <a href="#" className="link" ><span className="icon-Settings-2"></span></Link>
            <a href="#" className="link" ><span className="icon-Mail"></span></Link>
            <a href="#" className="link" ><span className="icon-Lock-overturning"><span className="path1"></span><span className="path2"></span></span></Link>
      </div>*/}
      </aside>
     
      )
    }
  }
  
  
  export default Navigation;