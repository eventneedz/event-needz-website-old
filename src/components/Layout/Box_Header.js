import React from 'react';
import {NavDropdown} from 'react-bootstrap';
const BoxHeader = ({boxTitle})=>{
    return(
        <>
                                                                
           
                    <div className="box-header ">
                        <h4 className="box-title">{boxTitle}</h4>   
                        <ul className="box-controls pull-right d-md-flex d-none nav">
                            <li><button className="btn btn-primary-light px-10">View All</button>
                            </li>
                            <li> 
                                <NavDropdown title="Most Popular">
                                <NavDropdown.Item href="#Today">Today</NavDropdown.Item>
                                <NavDropdown.Item href="#Yesterday">Yesterday</NavDropdown.Item>
                                <NavDropdown.Item href="#Last_Week">Last Week</NavDropdown.Item>
                                <NavDropdown.Item href="#Last_Month">Last Month</NavDropdown.Item>
                                </NavDropdown>
                            </li>
                            
                        </ul>
                       
                    </div>
            
           
            
        </>
    )
    

}
export default BoxHeader;