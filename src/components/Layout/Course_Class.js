import React from 'react';
const CourseClass = ({CclassTitle,CclassLink,CclassImg})=>{
    return(
        <>
            <div className="box pull-up">
                <div className="box-body d-flex align-items-center">
                    <img src={CclassImg} alt="" className="align-self-end h-80 w-80" />
                    <div className="d-flex flex-column flex-grow-1">
                        <h5 className="box-title font-size-16 mb-2">{CclassTitle}</h5>
                        <h5>{CclassLink}</h5>
                    </div>
                </div>
            </div>
        </>
    )
    

}
export default CourseClass;