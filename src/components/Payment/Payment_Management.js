import React from 'react';
import { Link } from 'react-router-dom';

function PaymentManagement () {
    return <>
        <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"> Payment Management </li>
							</ol>
						</nav>
					</div>
					
				</div>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">Payment Management</h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">
					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th>ID</th>
						  <th>Username</th>
						  <th>Type</th>
						  <th>Payment Address</th>
						  <th>Amount Requested</th>
						  <th>Remarks</th>
                          <th>Status</th>
                          <th>Date Created</th>
                          <th>ACTIONS</th>
						</tr>
						<tr>
						  <td>1</td>
						  <td>Cortie Gemson</td>
						  <td>Card </td>
						  <td>Brazil</td>
                          <td>$239,00</td>
                          <td>-</td>
						  <td><span className="badge badge-pill badge-success">Success</span></td>
                          <td>May 15, 2021</td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link></td>
						</tr>
						<tr>
						  <td>2</td>
						  <td>Cortie Gemson</td>
						  <td>Card </td>
						  <td>Brazil</td>
                          <td>$239,00</td>
                          <td>-</td>
						  <td><span className="badge badge-pill badge-danger">Cancel</span></td>
                          <td>May 15, 2021</td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link></td>
						</tr>
                        <tr>
						  <td>3</td>
						  <td>Cortie Gemson</td>
						  <td>Card </td>
						  <td>Brazil</td>
                          <td>$239,00</td>
                          <td>-</td>
						  <td><span className="badge badge-pill badge-warning">Pending</span></td>
                          <td>May 15, 2021</td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link></td>
						</tr>
						<tr>
						  <td>4</td>
						  <td>Cortie Gemson</td>
						  <td>Card </td>
						  <td>Brazil</td>
                          <td>$239,00</td>
                          <td>-</td>
						  <td><span className="badge badge-pill badge-warning">Pending</span></td>
                          <td>May 15, 2021</td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link></td>
						</tr>
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
    </>
}
export default PaymentManagement;