import React, {useState, useEffect } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
const DatatablePage = () => {
    const [users, setUsers] = useState([]);
	const getUsers = async () => {
		const response = await fetch('https://www.eventneedz.com/api/customers');
		
		setUsers(await response.json());
		//console.log(data);

	}
	useEffect(() => {
		getUsers();
	},[]);
  const data = {
    columns: [
      {
        label: 'Id',
        field: 'id',
        sort: 'asc',
        width: 150
      },
      {
        label: 'UserId',
        field: 'userid',
        sort: 'asc',
        width: 270
      },
      {
        label: 'Password',
        field: 'password',
        sort: 'asc',
        width: 200
      }
      
    ],
    rows: [
      {
        id: 'Tiger Nixon',
        userid: 'System Architect',
        password: 'Edinburgh',
       
      },
      {
        id: 'Tiger Nixon',
        userid: 'System Architect',
        password: 'Edinburgh',
       
      },
      
      
    ]
  };

  return (

    <>
    {
							users.map((curElem) =>{
								
								return(
									<tr>
									<td>{curElem.id}</td>
									<td>{curElem.loginId}</td>
									<td>{curElem.password}</td>
									<td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
									</tr>
								)

							})
						}
    <MDBDataTable
      striped
      bordered
      small
      data={data}
    />
    </>
  );
}

export default DatatablePage;