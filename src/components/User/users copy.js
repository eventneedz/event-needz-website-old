





import React from 'react';
import { Link } from 'react-router-dom';
function AddUser () {
    return <>
        <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"><Link to="/User"> User </Link></li>
								<li className="breadcrumb-item"> Add User  </li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
		<div className="col-lg-12 col-12">
        <div className="box">
						
						<form>
							<div className="box-body">
								<h4 className="mt-0 mb-20"> User Info:</h4>
								<div className="form-group">
									<label>First Name:</label>
									<input type="text" id='firstName' className="form-control" placeholder="Enter Firstname"/>
								</div>
								<div className="form-group">
									<label>Last Name:</label>							
									<input type="tel" id='lastName' className="form-control" placeholder="Enter LastName"/>
								</div>
								<div className="form-group">
									<label>Email address:</label>
									<input type="email" id='email' className="form-control" placeholder="Enter email"/>
								</div>
								<div className="form-group">
									<label>Password:</label>							
									<input type="Password" id='password' className="form-control" placeholder="Enter Password"/>
								</div>
								<div className="form-group">
									<label>Confirm Password:</label>							
									<input type="password" id='password' className="form-control" placeholder="Enter Password"/>
								</div>
								<div className="form-group">
									<label>Contact:</label>							
									<input type="number" id='mobile' className="form-control" placeholder="Enter Phone number"/>
								</div>
								{/* <div className="form-group">
									<label>Communications :</label>
									<div className="c-inputs-stacked">
										<input type="checkbox" id="checkbox_123"/>
										<label for="checkbox_123" className="block" style={{marginRight: "20px"}}>Email</label>
										<input type="checkbox" id="checkbox_234"/>
										<label for="checkbox_234" className="block" style={{marginRight: "20px"}}>SMS</label>
										<input type="checkbox" id="checkbox_345"/>
										<label for="checkbox_345" className="block" style={{marginRight: "20px"}}>Phone</label>
									</div>
								</div> */}
								
								{/* <h4 className="mt-0 mb-20">2. Payment Info:</h4> */}

								{/* <div className="form-group">
									<label>Payment Method :</label>
									<div className="c-inputs-stacked">
										<input name="group123" type="radio" id="radio_123" value="1"/>
										<label for="radio_123" className="mr-30">Credit Card</label>
										<input name="group456" type="radio" id="radio_456" value="1"/>
										<label for="radio_456" className="mr-30">Cash</label>
										<input name="group789" type="radio" id="radio_789" value="1"/>
										<label for="radio_789" className="mr-30">Wallet</label>
									</div>
								</div> */}
								{/* <div className="form-group">
									<label>Amount:</label>
									<input type="email" className="form-control" placeholder="Enter Amount"/>
								</div> */}
							</div>
							<div className="box-footer">
								<button type="submit" className="btn btn-danger">Cancel</button>
								<button type="submit" className="btn  btn-success pull-right">Submit</button>
							</div>
						</form>
					  </div>
				</div>
                </div>
                </section>
		
    </>
}
export default AddUser;