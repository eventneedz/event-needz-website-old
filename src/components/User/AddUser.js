import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import alertify from 'alertifyjs';
import 'alertifyjs/build/css/alertify.css';
// import {
// 	minMaxLength,
// 	validEmail,
// 	passwordStrength,
// 	userExists,
//   } from './validations';
function AddUsers() {
	const [firstName, setfirstName] = useState("");
	const [lastName, setlastName] = useState("");
	const [email, setemail] = useState("");
	const [password, setpassword] = useState("");
    const [re_password, setre_password] = useState("");
    const [mobail, setmobail] = useState("");
	const [formErrors, setFormErrors] = useState({});
	async function addinfo() {
		let item = { firstName, lastName, email, password,mobail }
		console.warn(item);
		let result = await fetch("https://eventneedz.com/api/customers", {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json"
			},
			body: JSON.stringify(item)
		})
		result = await result.json();
		console.log("test", result.status)
		if (result.status=='ACTIVE') {
			
			alertify.success(' Successfull Added ');
		}
		console.warn(result);

		setfirstName("");
		setlastName("");
		setemail("");
		setpassword("");
		setre_password("");
        setmobail("");

	}
	return <>
		<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"><Link to="/user"> User </Link></li>
								<li className="breadcrumb-item">Add User</li>
							</ol>
						</nav>
					</div>
				</div>

			</div>
		</div>
		{/* <form noValidate> */}
		<section className="content">
			<div className="row">
				<div className="col-lg-12 col-12">
					<div className="box">
						<div className="box-body">
							<h4 className="mt-0 mb-20">Add User :</h4>
							<div className="form-group">
								<label> FirstName:</label>
								<input type="text" value={firstName} onChange={(e) => setfirstName(e.target.value)} placeholder='firstName' className='form-control' required />
							</div>
							<div className="form-group">
								<label> LastName:</label>
								<input type="text" value={lastName} onChange={(e) => setlastName(e.target.value)} placeholder='lastName' className='form-control' required />
							</div>
							<div className="form-group">
								<label>UserName:</label>
								<input type="text" value={email} onChange={(e) => setemail(e.target.value)} placeholder='email' className='form-control' required />
							</div>
							<div className="form-group">
								<label> password:</label>
								<input type="text" value={password} onChange={(e) => setpassword(e.target.value)} placeholder='password' className='form-control' required />
							</div>
                            <div className="form-group">
								<label>Re-password:</label>
								<input type="text" value={re_password} onChange={(e) => setre_password(e.target.value)} placeholder='Re-password' className='form-control' required />
							</div>
                            <div className="form-group">
								<label> Contact:</label>
								<input type="text" value={mobail} onChange={(e) => setmobail(e.target.value)} placeholder='Contact' className='form-control' required />
							</div>
						</div>
						<div className="box-footer">
							<button type="submit" className="btn btn-warning" style={{ marginRight: "10px" }}>Reset</button>
							<button className="btn  btn-primary" onClick={addinfo}>Add User</button>
						</div>

					</div>
				</div>
			</div>
		</section>
		{/* </form> */}
	</>
}
export default AddUsers;
