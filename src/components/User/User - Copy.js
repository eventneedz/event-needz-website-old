import React from 'react';
import { Link } from 'react-router-dom';
import user1 from '../../images/avatar/avatar-1.png';
import user2 from '../../images/avatar/avatar-2.png';
import user3 from '../../images/avatar/avatar-3.png';
import user4 from '../../images/avatar/avatar-4.png';
import user5 from '../../images/avatar/avatar-5.png';
function User () {
    return <>
        
		<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"> User List </li>
							</ol>
						</nav>
					</div>
					
				</div>
				<Link to="/AddUser" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Add User </Link>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">User List</h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">
					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th>ID</th>
						  <th>PHOTO</th>
						  <th>FULLNAME</th>
						  <th>EMAIL</th>
						  <th>COUNTRY</th>
						  <th>STATUS</th>
                          <th>ACTIONS</th>
						</tr>
						<tr>
						  <td>1</td>
						  <td><span className='bg-lightest h-50 w-50 l-h-50 rounded-circle d-block'><img src={user1} alt=''/></span></td>
						  <td><span className="text-muted">Arlan Pond</span> </td>
						  <td>apond0@nytimes.com</td>
                          <td>Brazil</td>
						  <td><span className="badge badge-pill badge-danger">Active</span></td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
						</tr>
						<tr>
						  <td>2</td>
						  <td><span className='bg-lightest h-50 w-50 l-h-50 rounded-circle d-block'><img src={user2} alt=''/></span></td>
						  <td><span className="text-muted">Billi Cicero</span> </td>
						  <td>bcicero1@wiley.com</td>
                          <td>Indonesia</td>
						  <td><span className="badge badge-pill badge-success">Pending</span></td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
						</tr>
                        <tr>
						  <td>3</td>
						  <td><span className='bg-lightest h-50 w-50 l-h-50 rounded-circle d-block'><img src={user3} alt=''/></span></td>
						  <td><span className="text-muted">Arlan Pond</span> </td>
						  <td>apond0@nytimes.com</td>
                          <td>Brazil</td>
						  <td><span className="badge badge-pill badge-danger">Active</span></td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
						</tr>
						<tr>
						  <td>4</td>
						  <td><span className='bg-lightest h-50 w-50 l-h-50 rounded-circle d-block'><img src={user4} alt=''/></span></td>
						  <td><span className="text-muted">Billi Cicero</span> </td>
						  <td>bcicero1@wiley.com</td>
                          <td>Indonesia</td>
						  <td><span className="badge badge-pill badge-success">Pending</span></td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
						</tr>
                        <tr>
						  <td>4</td>
						  <td><span className='bg-lightest h-50 w-50 l-h-50 rounded-circle d-block'><img src={user5} alt=''/></span></td>
						  <td><span className="text-muted">Billi Cicero</span> </td>
						  <td>bcicero1@wiley.com</td>
                          <td>Indonesia</td>
						  <td><span className="badge badge-pill badge-warning">Pending</span></td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
						</tr>
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
		
    </>
}
export default User;