import React, {useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Spinner from 'react-bootstrap/Spinner'

const UseEffectAPI = () => {
	const [users, setUsers] = useState([]);
	const getUsers = async () => {
		const response = await fetch('https://www.eventneedz.com/api/customers');
		
		setUsers(await response.json());
		$("#lodderGet").css("display", "none");
		//console.log(data);

	}
	useEffect(() => {
		getUsers();
	},[]);
	
    return <>
        
		<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"> User List </li>
							</ol>
						</nav>
					</div>
					
				</div>
				<Link to="/AddUser" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Add User </Link>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">User List <Spinner animation="grow"  variant="info" size="2px" style={{ display: "block" }} id="lodderGet" /></h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body ">
				
					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th>S.No</th>
						  <th>FirstName</th>
						  <th>LastName</th>
						  {/* <th>Email</th> */}
						 
                          <th>ACTIONS</th>
						</tr>

						{
							users.map((curElem,ind) =>{
								
								
								return(
									<tr>
									<td>{ind+1}</td>
									<td>{curElem.firstName}</td>
									<td>{curElem.lastName}</td>
									{/* <td>{curElem.email}</td> */}
									{/* <td>{curElem["contactInfo"][0]["type"]}</td> */}
									
									
									<td><Link to="#" className='badge badge-pill badge-warning'>
										<i className='fa fa-eye'></i></Link> <Link to="#" className='badge badge-pill badge-danger'>
											<i className='fa fa-trash'></i></Link></td>
									</tr>
								)

							})
						}
						
						
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
		
		
    </>
}
export default UseEffectAPI;