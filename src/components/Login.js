import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();
  useEffect(() => {

    if (localStorage.getItem('user-info')) {
      navigate('/User')
    }
  }, [])
  // function login() {
  //   console.warn("data", email, password)
  //   let item = { email, password }
  //   let result = fetch('https://www.eventneedz.com/api/users', {
  //     method: 'POST',
  //     headers: {
  //       "content-Type": "application/json",
  //       "Accept": "application/json"
  //     },
  //     body: JSON.stringify(item)

  //   });

  //   result = result.json();
  //   localStorage.setItem('user-info', JSON.stringify(result));
  //   navigate('/User')
  // }

  return (
    <div className='Login'>
      <div className='Login_inner'>
        {/* <h1>Let's Get Started</h1> */}
        <h4 className="text-center text-dark">Login With EventNeedz</h4>
      
        <form className="user needs-validation" id="login_form" novalidate>



          <div className="form-group">
            {/* <label for="username">Username:</label>
            <input
              type="text"
              onChange={(e) => setEmail(e.target.value)}
              className="form-control"
              placeholder="Enter username"
            /> */}
             <label for="username">Username:</label>
            <input className="form-control form-control-user"
              type="text" id="username" placeholder="Enter username..."
              Name="username" required></input>

          </div>



          <div className="form-group">
            <label for="password">Password:</label>
            {/* <input
              type="password"
              onChange={(e) => setPassword(e.target.value)}
              className="form-control"
              placeholder="Enter password"
            /> */}
            <input className="form-control form-control-user"
              type="password" id="password" placeholder="Password" name="password"
              required></input>
          </div>

          <div className='form-group login-failed' style={{display: "none", color: "red"}}>Login
            failed. Please try again...</div>

          <button className="btn btn-primary btn-block text-white btn-user"
            type="submit">Login</button>

        </form>
      </div></div>
  );

}
(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$(document).ready(function () {
  var apiHost = "https://www.eventneedz.com/api";



  $('#login_form').submit(function (event) {
    $("#loading").show();
    if (this.checkValidity()) {
      event.preventDefault();

      axios.get(apiHost + '/users', {
        params: {
          username: $('#username').val(),
          password: $('#password').val()
        }
      })
        .then(function (response) {
          if (response.data && response.data.id) {

            sessionStorage.setItem('usrses', JSON.stringify(response.data));
           //  var x = localStorage.getItem("mytime");
           location.href = "index.html";

          } else {
            $(".login-failed").show();
            $("#loading").hide();
          }
        })
        .catch(function (error) {
          if (error.response.status == 404) {
            $(".login-failed").show();
            $("#loading").hide();
          }
        });
    } else {
      $("#loading").hide();
    }
  });
});

export default Login;