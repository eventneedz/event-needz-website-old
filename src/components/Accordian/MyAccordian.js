import React from 'react';
const MyAccordian = ({id,question,answer}) => {
    return(
        <>
            <table className='table'>
                <tr>
                    <td>{id}</td>
                    <td>{question}</td>
                    <td>{answer}</td>
                </tr>
            </table>
        </>

    )
}
export default MyAccordian;