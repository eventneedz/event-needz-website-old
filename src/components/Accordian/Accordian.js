import React, {useState} from 'react';
import {questions} from './Api'
import MyAccordian from './MyAccordian';
const Accordian = () => {
    const [data,setData] = useState(questions);
    return (
        <>
        {
            data.map((curElem) =>{

                const {id} = curElem;
                return <MyAccordian key={id} {...curElem} />

            }
            
            
            )
        }
    </>

    )
}
export default Accordian;