import React from 'react';
import { Link } from 'react-router-dom';
function SendEnquery () {
    return <>
        <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"><Link to="/Enquery"> Enquery </Link></li>
								<li className="breadcrumb-item"> Send Enquery  </li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
		<div className="col-lg-12 col-12">
        <div className="box">
						
						<form>
						
								<div className='box-header with-border'>
									<div className='row'>
										<div className="col-lg-9 col-12">
											<h4 className="mt-0 mb-20"> Add Additional Infomation About Customer Enquery</h4>
										</div>
										<div className="col-lg-3 col-12">
											<select className="browser-default custom-select  btn-primary">
											<option>Enquery Id</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											</select>
										</div>
									</div>
								</div>
							
								<div className='box-header with-border enquery_info'>
									<div className='row '>
										<div className="col-lg-9 col-12">
											<p>Customer Name : <span>Vasanth</span></p>
											<p>Mobile : <span>+89756622</span></p>
											<p>Email : <span>customer@gmail.com</span></p>
										</div>
										<div className="col-lg-3 col-12">
											<p>Employee Name : <span>Vareesh</span></p>
											<p>Employee Id : <span>K006</span></p>
											<p>Bid Validity : <span>3Days</span></p>
										</div>
									</div>
									</div>
									<div className="box-body">
								
								<div className='row'>
									<div className='col-md-12'>
										<div className='row'>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Event Type </label>
													<select className="browser-default custom-select">
														<option>Choose your option</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														</select>
												</div>
											</div>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Category </label>
													<select className="browser-default custom-select">
														<option>Choose your option</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														</select>
												</div>
											</div>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Slot</label>
													<select className="browser-default custom-select">
														<option>Choose your option</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														</select>
												</div>
											</div>
										</div>
										<div className='row'>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Number Of Guests</label>
													<select className="browser-default custom-select">
														<option>Choose your option</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														</select>
												</div>
											</div>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Location </label>
													<select className="browser-default custom-select">
														<option>Choose your option</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														</select>
												</div>
											</div>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Event Date </label>
													<select className="browser-default custom-select">
														<option>Choose your option</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														</select>
												</div>
											</div>
										</div>
										<div className='row'>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Services</label>
													<select className="browser-default custom-select">
														<option>Choose your option</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														</select>
												</div>
											</div>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Bid Start Date </label>
													<input type="date" className="form-control" placeholder="Minimum Order Quantity"/>
												</div>
											</div>
											<div className='col-md-4'>
												<div className="form-group">
													<label>Bid End Date </label>
													<input type="date" className="form-control" placeholder="Quantity Step Size "/>
												</div>
											</div>
										</div>
										
									</div>
									
								</div>
								
								<div className="form-group">
									<label>Get extra information from customer about event:</label>
									<textarea className='form-control' placeholder="Q1. Question 1"></textarea>
									
								</div>
								
							</div>
							<div className='col-md-12'>
							<div className="table-responsive">
								<table className="table table-hover">
									<tbody>
										<tr>
										<th>Company Name</th>
										<th>Contact Person</th>
										<th>Contact Number</th>
										<th>Bid Amount</th>
										<th>Vendor Profile</th>
										<th> <input type="checkbox" id="checkbox_123"/>
										<label for="checkbox_123" className="block" style={{marginRight: "20px"}}>Select All</label></th>
										</tr>
										<tr>
										<td><input type="text" className='form-control'  placeholder='Akahaya Vendors' disabled/></td>
										<td>Subba Readdy</td>
										<td> +91 6546832323 </td>
										<td>&#8377; 200</td>
										<td><Link to="/EnqueryView" className='btn btn-primary'> View</Link></td>
									
										<td><input type="checkbox" id="checkbox_2"/>
										<label for="checkbox_2" className="block" style={{marginRight: "20px"}}></label> </td>
										</tr>
										<tr>
										<td><input type="text" className='form-control'  placeholder='Akahaya Vendors' disabled/></td>
										<td>Subba Readdy</td>
										<td> +91 6546832323 </td>
										<td>&#8377; 200</td>
										<td><Link to="/EnqueryView" className='btn btn-primary'> View</Link></td>
									
										<td><input type="checkbox" id="checkbox_21"/>
										<label for="checkbox_21" className="block" style={{marginRight: "20px"}}></label> </td>
										</tr>
										<tr>
										<td><input type="text" className='form-control'  placeholder='Akahaya Vendors' disabled/></td>
										<td>Subba Readdy</td>
										<td> +91 6546832323 </td>
										<td>&#8377; 200</td>
										<td><Link to="/EnqueryView" className='btn btn-primary'> View</Link></td>
									
										<td><input type="checkbox" id="checkbox_2"/>
										<label for="checkbox_22" className="block" style={{marginRight: "20px"}}></label> </td>
										</tr>
									</tbody>
								</table>
							</div>
							</div>
							<div className="box-footer">
								<button type="submit" className="btn  btn-primary">Submit</button>
							</div>
						</form>
					  </div>
				</div>
                </div>
                </section>
		
    </>
}
export default SendEnquery;