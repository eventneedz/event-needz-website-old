import React from 'react';
import { Link } from 'react-router-dom';
import user1 from '../../images/product/product-1.png';
import user2 from '../../images/product/product-2.png';
import user3 from '../../images/product/product-3.png';
import user4 from '../../images/product/product-4.png';
import user5 from '../../images/product/product-5.png';


function EnqueryView () {
    return <>
   
      <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"> Enquery View </li>
							</ol>
						</nav>
                        
					</div>
					
				</div>
				<Link to="/SendEnquery" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Send Enquery </Link>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
              <div className="box-body">
    <div className="row">
        
       
        <div className="col-lg-6 col-md-12 col-sm-12">
            <div className="table-responsive">
                <table className="table no-border enent-table">
                    <tbody>
                        <tr>
                            <td >Category</td>
                            <td> Decorator</td>
                        </tr>
                        <tr>
                            <td>Event Type</td>
                            <td> Birthday</td>
                        </tr>
                        <tr>
                            <td>Slot</td>
                            <td> 6pm - 12am </td>
                        </tr>
                        <tr>
                            <td>Number Of Guest</td>
                            <td> 100 </td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td> Ameerpet </td>
                        </tr>
                        <tr>
                            <td>Event Date</td>
                            <td> 15-Dec-2021 </td>
                        </tr>
                        <tr>
                            <td>services</td>
                            <td> <span className='badge badge-pill badge-danger'>1</span> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div className='your_choise'>Your Choice <span>(18% GST + &#8377; 25 Convenience Fee)</span></div>
            <div className="form-group">
                <label> extra information from customer about event:</label>
                <textarea className='form-control' placeholder="Q1. Question 1"></textarea>
                
            </div>
            <div className="table-responsive">
								<table className="table">
                                <thead class="thead-light">
										<tr>
										<th>Company Name</th>
										<th>Bid Price</th>
										</tr>
                                        </thead>
										<tr>
                                            <td><input type="text" className='form-control'  placeholder='Akahaya Vendors' /></td>
                                            <td>&#8377; 200</td>
										</tr>
										<tr>
                                            <td><input type="text" className='form-control'  placeholder='Akahaya Vendors' /></td>
                                            <td>&#8377; 200</td>
										</tr>
                                        <tr>
                                            <td><input type="text" className='form-control'  placeholder='Akahaya Vendors' /></td>
                                            <td>&#8377; 200</td>
										</tr>
                                       
									
								</table>
							</div>
        </div>
    </div>
</div>
			  </div>
			</div>
            </div>
        </section>
       
    </>
}
export default EnqueryView;