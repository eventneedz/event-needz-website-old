import React from 'react';
import { Link } from 'react-router-dom';
import user1 from '../../images/product/product-1.png';
import user2 from '../../images/product/product-2.png';
import user3 from '../../images/product/product-3.png';
import user4 from '../../images/product/product-4.png';
import user5 from '../../images/product/product-5.png';
 
function Leads () {
    return <>
        	<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item">Leads </li>
							</ol>
						</nav>
                        
					</div>
				</div>
				<Link to="/SendEnquery" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Send Enquery </Link>
				
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">Leads List</h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">
					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th>Category</th>
						  <th>Event Type</th>
						  <th>Slot</th>
						  <th>Number Of Guests</th>
						  <th>Loaction</th>
						  <th>Event Date</th>
                          <th>ACTIONS</th>
						</tr>
						<tr>
						  <td>Decorator</td>
						  <td>Birthday</td>
						  <td>6pm - 12 am </td>
						  <td>100</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/EnqueryView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>
						<tr>
						  <td>Decorator</td>
						  <td>Birthday</td>
						  <td>6pm - 12 am </td>
						  <td>100</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/EnqueryView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>
						<tr>
						  <td>Decorator</td>
						  <td>Birthday</td>
						  <td>6pm - 12 am </td>
						  <td>100</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/EnqueryView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>
						<tr>
						  <td>Decorator</td>
						  <td>Birthday</td>
						  <td>6pm - 12 am </td>
						  <td>100</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/EnqueryView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>
						<tr>
						  <td>Decorator</td>
						  <td>Birthday</td>
						  <td>6pm - 12 am </td>
						  <td>100</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/EnqueryView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>

						
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
       
    </>
}
export default Leads;