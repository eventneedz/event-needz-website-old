import React from 'react';
import { Link } from 'react-router-dom';
import user1 from '../../images/product/product-1.png';
import user2 from '../../images/product/product-2.png';
import user3 from '../../images/product/product-3.png';
import user4 from '../../images/product/product-4.png';
import user5 from '../../images/product/product-5.png';
 
function Enquery () {
    return <>
        	<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item">Enquery </li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">Enquery List</h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">
					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th>Customer Name</th>
						  <th>Mobile No.</th>
						  <th>Email</th>
						  <th>Event Type</th>
						  <th>Loaction</th>
						  <th>Event Date</th>
                          <th>ACTIONS</th>
						</tr>
						<tr>
						  <td>Vasanth</td>
						  <td>+89756622</td>
						  <td>customer@gmail.com</td>
						  <td>Decorator</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/SendEnquery" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>
						<tr>
						  <td>Vasanth</td>
						  <td>+89756622</td>
						  <td>customer@gmail.com</td>
						  <td>Decorator</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/SendEnquery" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>
						<tr>
						  <td>Vasanth</td>
						  <td>+89756622</td>
						  <td>customer@gmail.com</td>
						  <td>Decorator</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/SendEnquery" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>
						<tr>
						  <td>Vasanth</td>
						  <td>+89756622</td>
						  <td>customer@gmail.com</td>
						  <td>Decorator</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/SendEnquery" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>

						<tr>
						  <td>Vasanth</td>
						  <td>+89756622</td>
						  <td>customer@gmail.com</td>
						  <td>Decorator</td>
                          <td>Ameerpet</td>
						  <td>02/03/2021</td>
						  <td><Link to="/SendEnquery" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> </td>
						</tr>

						
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
       
    </>
}
export default Enquery;