import React, { useState, useEffect } from 'react';
import Spinner from 'react-bootstrap/Spinner'


import { Link } from 'react-router-dom';
// import { Image } from  "../../../public/images/lodder.js";




const Vendors = () => {
	var lodder = Spinner
	//  var image = Image;
	const [users, setUsers] = useState([]);

	const getUsers = async () => {

		const response = await fetch('https://www.eventneedz.com/api/vendors');
	

		/* if(response!=null || response==undefined || respones=="")
		{
			var notDataFound='No Data found';
		}
		else
		{
			$("#lodderGet").css("display", "none");

			
		} */

		setUsers(await response.json());
		$("#lodderGet").css("display", "none");
		

		//console.log(data);

	}
	useEffect(() => {
		getUsers();
	}, []);

	return <>

		<div className="content-header">


			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">

								<li className="breadcrumb-item" >Vendors List</li>
							</ol>
						</nav>
					</div>

				</div>
				<Link to="/AddVendor" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Add Vendors </Link>

			</div>
		</div>
		<section className="content">
			<div className="row">
				<div className="col-12">
					<div className="box">
						<div className="box-header with-border">
							<h4 className="box-title">Vendors List  <Spinner animation="grow"  variant="info" size="2px" style={{ display: "block" }} id="lodderGet" /> </h4>
							<div className="box-controls pull-right">
								<div className="lookup lookup-circle lookup-right">
									<input type="text" name="s" />
								</div>
							</div>
						</div>
						<div className="box-body no-padding">
							
							<div className="table-responsive">
								<table className="table table-hover">
									<tbody><tr>
										<th>ID</th>
										<th>FULLNAME</th>

										<th>Company Name</th>
										<th>DATE</th>
										<th>STATUS</th>
										<th>ACTIONS</th>
									</tr>

										{
											users.map((curElem) => {
												console.log(curElem);
												return (
													<tr>
														<td>{curElem.id}</td>
														<td>{curElem.contactName}</td>
														<td>{curElem.companyName}</td>
														<td>{curElem.creationDate}</td>
														<td>{curElem.status}</td>
														<td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
													</tr>
												)

											})
										}


									</tbody></table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


	</>
}
export default Vendors;