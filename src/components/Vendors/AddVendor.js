
import React, { useState } from 'react'
import logo from '../../images/logo.png';
import { Link } from "react-router-dom";
import alertify from 'alertifyjs';
import 'alertifyjs/build/css/alertify.css';



function VendorSignup() {
  const [cname, setCname] = useState("");
  const [name, setName] = useState("");
  const [tel, setTel] = useState("");
  const [email, setEmail] = useState("");
  const [category, setCategory] = useState("");
  const [address, setAddress] = useState("");
  const [services, setServices] = useState("");
  const [status, setStatus] = useState("");


  async function signup() {
    let item = { cname, name, tel, email, category, address, services, status }
    console.warn(item);
    let result = await fetch("https://www.eventneedz.com/api/vendors", {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      },
      body: JSON.stringify(item)
    })
    result = await result.json();
    console.log("test", result.id)
		if (result.id) {
			
			alertify.success('Vendor Added Successfully Please wait for verification');
		}
    console.warn(result);

    setCname("")
    setName("")
    setTel("")
    setEmail("")
    setCategory("")
    setServices("")
    setAddress("")
    setStatus("")

  }
  return (
    <>
      <div className='col-sm-6 offset-sm-3 mt-50'>
        <div className='V_login'>
         
          <div className='row'>
            <div className='col-md-6'>
              <div className="form-group">
                <label> Name:</label>
                <input type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder='Contact Name' className='form-control' required />
              </div>
            </div>
            <div className='col-md-6'>
              <div className="form-group">
                <label> Company Name:</label>
                <input type="text" value={cname} required onChange={(e) => setCname(e.target.value)} placeholder='Company Name'required className='form-control' />
              </div>
            </div>
            <div className='col-md-6'>
              <div className="form-group">
                <label> Contact No:</label>
                <input type="tel" value={tel} required onChange={(e) => setTel(e.target.value)} placeholder='Contact No' required className='form-control' />
              </div>
            </div>
            <div className='col-md-6'>
              <div className="form-group">
                <label> Email:</label>
                <input type="text" value={email}  onChange={(e) => setEmail(e.target.value)} placeholder='Email'  className='form-control' required />
              </div>
            </div>
            <div className='col-md-6'>
              <div className="form-group">
                <label> Category:</label>
                <select name="category" class="custom-select"  onChange={(e) => setCategory(e.target.value)} >
                  <option value="default" >Select Category</option>
                  <option value="Advertise" onChange={(e) => setCategory(e.target.value)}>Advertise</option>
                  <option value="Anchor" onChange={(e) => setCategory(e.target.value)}>Anchor</option>
                  <option value="Arabian Cuisines" onChange={(e) => setCategory(e.target.value)}>Arabian Cuisines</option>
                  <option value="Artist Management" onChange={(e) => setCategory(e.target.value)}>Artist Management</option>

                </select>
              </div>
            </div>
            <div className='col-md-6'>
              <div className="form-group">
                <label> Address:</label>
                <input type="text" value={address} onChange={(e) => setAddress(e.target.value)} placeholder='Address' className='form-control' />
              </div>
            </div>
            <div className='col-md-6'>
              <div className="form-group">
                <label> Services:</label>
                <input type="text" value={services} onChange={(e) => setServices(e.target.value)} placeholder='Services' className='form-control' />
              </div>
            </div>
            <div className='col-md-6'>
              <div className="form-group">
                <label> status:</label>
                <input type="text" value={status} onChange={(e) => setStatus(e.target.value)} placeholder='Status' className='form-control' />
              </div>
            </div>
            <div className='col-md-6'><button onClick={signup} className='btn btn-primary'>Send</button></div>
            {/* <div className='col-md-6 text-right'> <Link to="/Login">Already have an account? Log In</Link></div> */}
          </div>





        </div>
      </div>
    </>
  )
}

export default VendorSignup
