import React from 'react';
import { Link } from 'react-router-dom';

function Report () {
    return <>
      <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"> Reports </li>
							</ol>
						</nav>
					</div>
					
				</div>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">Reports</h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">

           

					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th>Order Id</th>
						  <th>Username</th>
						  <th>Mobile</th>
						  <th> Address</th>
						  <th>Final Total(₹)</th>
              <th>Status</th>
              <th>Date </th>
              <th>ACTIONS</th>
						</tr>
						<tr>
						  <td>1</td>
						  <td>Cortie Gemson</td>
						  <td>9479447720 </td>
						  <td>Indore, Palasia, Indore, Madhya Pradesh, India, 452001</td>
               <td>$239,00</td>
						  <td><span className="badge badge-pill badge-success">Success</span></td>
                          <td>May 15, 2021</td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link></td>
						</tr>
            <tr>
						  <td>2</td>
						  <td>Cortie Gemson</td>
						  <td>9479447720 </td>
						  <td>Indore, Palasia, Indore, Madhya Pradesh, India, 452001</td>
               <td>$239,00</td>
						  <td><span className="badge badge-pill badge-success">Success</span></td>
                          <td>May 15, 2021</td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link></td>
						</tr>
            <tr>
						  <td>3</td>
						  <td>Cortie Gemson</td>
						  <td>9479447720 </td>
						  <td>Indore, Palasia, Indore, Madhya Pradesh, India, 452001</td>
               <td>$239,00</td>
						  <td><span className="badge badge-pill badge-success">Success</span></td>
                          <td>May 15, 2021</td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link></td>
						</tr>
            <tr>
						  <td>4</td>
						  <td>Cortie Gemson</td>
						  <td>9479447720 </td>
						  <td>Indore, Palasia, Indore, Madhya Pradesh, India, 452001</td>
               <td>$239,00</td>
						  <td><span className="badge badge-pill badge-success">Success</span></td>
                          <td>May 15, 2021</td>
						  <td><Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link></td>
						</tr>
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
    </>
}
export default Report;