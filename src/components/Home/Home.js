import React from 'react';
import BoxHeader from '../Layout/Box_Header';
import CourseClass from '../Layout/Course_Class';
import CoursBox from '../Layout/Cours_Box';
import icon1 from '../../images/front-end-img/courses/cor-logo-6.png';
import icon2 from '../../images/front-end-img/courses/cor-logo-3.png';
import icon3 from '../../images/front-end-img/courses/cor-logo-4.png';
import icon4 from '../../images/front-end-img/courses/cor-logo-5.png';
import icon5 from '../../images/front-end-img/courses/1.png';
import icon6 from '../../images/front-end-img/courses/2.png';
import icon7 from '../../images/front-end-img/courses/3.png';
import icon8 from '../../images/front-end-img/courses/4.png';
 
function Home (){
     return <>
      <section className="content">
     
   <div class="row">
    
     <div className="col-md-3"><CourseClass  CclassTitle='Total Products' CclassLink='100' CclassImg={icon4}/></div>
     <div className="col-md-3"><CourseClass  CclassTitle='Total Customer' CclassLink='200' CclassImg={icon1}/></div>
     <div className="col-md-3"><CourseClass  CclassTitle='Total Orders Executed' CclassLink='250' CclassImg={icon3}/></div>
     <div className="col-md-3"><CourseClass  CclassTitle='Orders In Process' CclassLink='30' CclassImg={icon2}/></div>
 </div>
 <div className="row fx-element-overlay"> 
    <div className="col-lg-6 col-12">
      <div className="box ">
      <div className="box-header">
							<h4 className="box-title">Popular Category</h4>
							<h5 className='pull-right'>Total Category : 100</h5>
						</div>
        <div className='box-body'>
          <div className="row"> 
            <div className="col-lg-6 col-md-6 col-12"><CoursBox  courseTitle='Haldi' courseLink='#Haldi' courseImg={icon5}/></div>
            <div className="col-lg-6 col-md-6 col-12"><CoursBox  courseTitle='Anchor' courseLink='#Anchor' courseImg={icon6}/></div>
            <div className="col-lg-6 col-md-6 col-12"><CoursBox  courseTitle='Birthday' courseLink='#Birthday' courseImg={icon7}/></div>
            <div className="col-lg-6 col-md-6 col-12"><CoursBox  courseTitle='Caterer' courseLink='#Caterer' courseImg={icon8}/></div>
          </div>
        </div>
      </div>
    </div>
    <div className="col-lg-6 col-12">
      <div className="box ">
        <BoxHeader  boxTitle='Analytics'  />
          <div className='box-body'>
          <div id="charts_widget_1_chart"></div>
          </div>
        </div>
    </div>
</div>
  </section>
</>
}
 
export default Home;