import React from 'react';
class HomeCont extends React.Component{
    render(){
    return(
        <>
        < div className="col-12">
            <div className="row">
				<div className="col-xl-8 col-12">
					<div className="row">
                        <div className="col-xl-5 col-lg-6 col-12">
									<div className="box">
										<div className="box-header">
											<h4 className="box-title">Course completion</h4>
											<ul className="box-controls pull-right d-md-flex d-none">
											  <li>
												<button className="btn btn-primary-light px-10">View All</button>
											  </li>
											</ul>
										</div>
										<div className="box-body">
											<div className="mb-25">
												<div className="d-flex align-items-center justify-content-between">
													<div className="w-p85">
														<div className="progress progress-sm mb-0">
															<div className="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{width: "40%"}}>
															</div>
														</div>
													</div>
													<div>
														<div>40%</div>
													</div>
												</div>
												<div className="d-flex align-items-center justify-content-between">
													<p className="mb-0 text-primary">In Progress</p>
													<p className="text-fade mb-0">117 User</p>
												</div>
											</div>
											<div className="mb-25">
												<div className="d-flex align-items-center justify-content-between">
													<div className="w-p85">
														<div className="progress progress-sm mb-0">
															<div className="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style={{width: "20%"}}>
															</div>
														</div>
													</div>
													<div>
														<div>20%</div>
													</div>
												</div>
												<div className="d-flex align-items-center justify-content-between">
													<p className="mb-0 text-primary">Completed</p>
													<p className="text-fade mb-0">74 User</p>
												</div>
											</div>
											<div className="mb-25">
												<div className="d-flex align-items-center justify-content-between">
													<div className="w-p85">
														<div className="progress progress-sm mb-0">
															<div className="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100" style={{width: "40%"}}>
															</div>
														</div>
													</div>
													<div>
														<div>18%</div>
													</div>
												</div>
												<div className="d-flex align-items-center justify-content-between">
													<p className="mb-0 text-primary">Inactive</p>
													<p className="text-fade mb-0">58 User</p>
												</div>
											</div>
											<div className="mb-25">
												<div className="d-flex align-items-center justify-content-between">
													<div className="w-p85">
														<div className="progress progress-sm mb-0">
															<div className="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style={{width: "7%"}}>
															</div>
														</div>
													</div>
													<div>
														<div>07%</div>
													</div>
												</div>
												<div className="d-flex align-items-center justify-content-between">
													<p className="mb-0 text-primary">Expeired</p>
													<p className="text-fade mb-0">11 User</p>
												</div>
											</div>
											<div className="mb-25">
												<div className="d-flex align-items-center justify-content-between">
													<div className="w-p85">
														<div className="progress progress-sm mb-0">
															<div className="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{width: "40%"}}>
															</div>
														</div>
													</div>
													<div>
														<div>40%</div>
													</div>
												</div>
												<div className="d-flex align-items-center justify-content-between">
													<p className="mb-0 text-primary">In Progress</p>
													<p className="text-fade mb-0">117 User</p>
												</div>
											</div>
											<div className="mb-5">
												<div className="d-flex align-items-center justify-content-between">
													<div className="w-p85">
														<div className="progress progress-sm mb-0">
															<div className="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style={{width: "20%"}}>
															</div>
														</div>
													</div>
													<div>
														<div>20%</div>
													</div>
												</div>
												<div className="d-flex align-items-center justify-content-between">
													<p className="mb-0 text-primary">Completed</p>
													<p className="text-fade mb-0">74 User</p>
												</div>
											</div>
										</div>
									</div>
								</div>
                                <div className="col-xl-7 col-lg-6 col-12">
									<div className="box bg-transparent no-shadow mb-30">
										<div className="box-header no-border pb-0">
											<h4 className="box-title">Lessons</h4>
											<ul className="box-controls pull-right d-md-flex d-none">
											  <li>
												<button className="btn btn-primary-light px-10">View All</button>
											  </li>
											</ul>
										</div>
									</div>
									<div className="box mb-15 pull-up">
										<div className="box-body">
											<div className="d-flex align-items-center justify-content-between">
												<div className="d-flex align-items-center">
													<div className="mr-15 bg-warning h-50 w-50 l-h-60 rounded text-center">
														<span className="icon-Book-open font-size-24"><span className="path1"></span><span className="path2"></span></span>
													</div>
													<div className="d-flex flex-column font-weight-500">
														<a href="#" className="text-dark hover-primary mb-1 font-size-16">Informatic Course</a>
														<span className="text-fade">Johen Doe, 19 April</span>
													</div>
												</div>
												<a href="#">
													<span className="icon-Arrow-right font-size-24"><span className="path1"></span><span className="path2"></span></span>
												</a>
											</div>
										</div>
									</div>
									<div className="box mb-15 pull-up">
										<div className="box-body">
											<div className="d-flex align-items-center justify-content-between">
												<div className="d-flex align-items-center">
													<div className="mr-15 bg-primary h-50 w-50 l-h-60 rounded text-center">
														<span className="icon-Mail font-size-24"></span>
													</div>
													<div className="d-flex flex-column font-weight-500">
														<a href="#" className="text-dark hover-primary mb-1 font-size-16">Live Drawing</a>
														<span className="text-fade">Micak Doe, 12 June</span>
													</div>
												</div>
												<a href="#">
													<span className="icon-Arrow-right font-size-24"><span className="path1"></span><span className="path2"></span></span>
												</a>
											</div>
										</div>
									</div>
									<div className="box mb-15 pull-up">
										<div className="box-body">
											<div className="d-flex align-items-center justify-content-between">
												<div className="d-flex align-items-center">
													<div className="mr-15 bg-danger h-50 w-50 l-h-60 rounded text-center">
														<span className="icon-Book-open font-size-24"><span className="path1"></span><span className="path2"></span></span>
													</div>
													<div className="d-flex flex-column font-weight-500">
														<a href="#" className="text-dark hover-primary mb-1 font-size-16">Contemporary Art</a>
														<span className="text-fade">Potar doe, 27 July</span>
													</div>
												</div>
												<a href="#">
													<span className="icon-Arrow-right font-size-24"><span className="path1"></span><span className="path2"></span></span>
												</a>
											</div>
										</div>
									</div>
									<div className="box mb-15 pull-up">
										<div className="box-body">
											<div className="d-flex align-items-center justify-content-between">
												<div className="d-flex align-items-center">
													<div className="mr-15 bg-info h-50 w-50 l-h-60 rounded text-center">
														<span className="icon-Mail font-size-24"></span>
													</div>
													<div className="d-flex flex-column font-weight-500">
														<a href="#" className="text-dark hover-info mb-1 font-size-16">Live Drawing</a>
														<span className="text-fade">Micak Doe, 12 June</span>
													</div>
												</div>
												<a href="#">
													<span className="icon-Arrow-right font-size-24"><span className="path1"></span><span className="path2"></span></span>
												</a>
											</div>
										</div>
									</div>
								</div>	
                    </div>
                </div>
                <div className="col-xl-4 col-12">
							<div className="box">
								<div className="box-body">							
									<div id="calendar" className="dask evt-cal min-h-400"></div>
								</div>
							</div>
						</div>
            </div>
        </div>
        </>
    )
    

}
}
export default HomeCont;