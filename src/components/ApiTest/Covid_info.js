import {Col} from 'react-bootstrap';
import React from 'react';
import icon5 from '../../images/icon5.png';

const CovidInfo = ({cardTitle,value,desc,lastUpdate,iconimg})=>{
    return(
        <>
            <Col>
              <div className="card_box">
                <div className="icon_box"> <img src={icon5} alt='Covid' /></div>
                <h3>{cardTitle }</h3>
                <h5>{value}</h5>
                <p>{desc}</p>
              </div>
            </Col>
        </>
    )
    

}
export default CovidInfo;