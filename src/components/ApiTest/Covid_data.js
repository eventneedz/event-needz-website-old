import CovidInfo from './Covid_info';
import {Row, Container} from 'react-bootstrap';
import React from 'react';

function CovidData({data:{confirmed,recovered,deaths,lastUpdate}}){
    if(!confirmed){
       return ""
    }

  

    return(
        <>
        <div className="top-section">
         <div className="corona-title">
          <h1 className="text-center title-covid">COVID-19 Live Update</h1>
            <Container>
                <Row>
                    <h4 className="text-center title-covid">{new Date(lastUpdate).toDateString()}</h4>
                        <CovidInfo 
                        cardTitle='ConFirm'
                        value={confirmed.value}
                        desc='Number of ConFirm Cases of Covid-19'
                        lastUpdate={lastUpdate}
                        />
                          <CovidInfo 
                           cardTitle='Recovered'
                        value={recovered.value}
                        desc='Number of Recovered Cases from Covid-19'
                        lastUpdate={lastUpdate}
                        />
                        <CovidInfo
                          cardTitle='Deaths'
                          value={deaths.value}
                          desc='Number of Deaths Cases by Covid-19'
                          lastUpdate={lastUpdate}
                        />
                </Row>
            </Container>
        </div></div>
        </>
    
    )
}
export default CovidData; 