import React,{useState,useEffect} from 'react';
 
const DataTest = () =>{

    const[users, setUsers] = useState([]) ;
    const getUsers = async() =>{
        const response = await fetch('http://159.65.89.148:3016/products/category');
        setUsers(await response.json());
        //console.log(data);
    }
    useEffect(() => {
        getUsers();
    },[]);

    return (
        <>
                <table className='table'>
                    {

                        users.map((curElem) => {

                            return(

                                <tr>
                                    <td>{curElem.id}</td>
                                    <td>{curElem.name}</td>
                                    <td>{curElem.slug}</td>
                                    <td><img src={curElem.image} /></td>
                                   

                                </tr>
                            );

                        })

                    }
                    
                </table>
        </>
        )
}
 
export default DataTest;