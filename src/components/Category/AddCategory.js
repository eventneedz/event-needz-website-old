import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import alertify from 'alertifyjs';
import 'alertifyjs/build/css/alertify.css';



function AddCategory() {
	const [name, setName] = useState("");
	const [city, setCity] = useState("");
	const [seoName, setSeoName] = useState("");
	const [slotName, setSlotName] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");
	async function addinfo() {
		let item = { name, city, seoName, slotName,description,image }
		console.warn(item);
		let result = await fetch("https://www.eventneedz.com/api/events/category", {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json"
			},
			body: JSON.stringify(item)
		})
		result = await result.json();
		console.log("test", result.id)
		if (result.id) {
			
			alertify.success('Your Category Successfull Added Please wait for verification ');
		}
		console.warn(result);

		setName("");
		setCity("");
		setSeoName("");
		setSlotName("");
		setDescription("");
		setImage("")


	}
	return <>
		<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"><Link to="/Category"> Category </Link></li>
								<li className="breadcrumb-item">Add Category</li>
							</ol>
						</nav>
					</div>
				</div>

			</div>
		</div>
		<section className="content">
			<div className="row">
				<div className="col-lg-12 col-12">
					<div className="box">
						<div className="box-body">
							<h4 className="mt-0 mb-20">Add Category :</h4>
							<div className="form-group">
								<label> Name:</label>
								<input type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder='Contact Name' className='form-control' required />
							</div>
							<div className="form-group">
								<label> description:</label>
								<input type="text" value={description} onChange={(e) => setDescription(e.target.value)} placeholder='Description' className='form-control' required />
							</div>
							<div className="form-group">
								<label> Image:</label>
								<input type="file" value={image} onChange={(e) => setImage(e.target.value)} placeholder='Image' className='form-control' required />
							</div>
							<div className="form-group">
								<label> City:</label>
								<input type="text" value={city} onChange={(e) => setCity(e.target.value)} placeholder='City' className='form-control' required />
							</div>
							<div className="form-group">
								<label> Seo Name:</label>
								<input type="text" value={seoName} onChange={(e) => setSeoName(e.target.value)} placeholder='Seo Name' className='form-control' required />
							</div>
							<div className="form-group">
								<label> Sloat Name:</label>
								<input type="text" value={slotName} onChange={(e) => setSlotName(e.target.value)} placeholder='Sloat Name' className='form-control' required />
							</div>
							
						</div>
						<div className="box-footer">
							<button type="submit" className="btn btn-warning" style={{ marginRight: "10px" }}>Reset</button>
							<button className="btn  btn-primary" onClick={addinfo}>Add Category</button>
						</div>

					</div>
				</div>
			</div>
		</section>
	</>
}
export default AddCategory;
