import React, {useState, useEffect } from 'react';
import { Link ,useParams} from 'react-router-dom';
function UpdateCategory (props) {
	const params = useParams();
	const [data, setData] = useState([]);
	const [name,setName]=useState("");
 	const [city,setCity]=useState("");
  const [seoName,setSeoName]=useState("");
  const [slotName,setSlotName]=useState("");

   
      const getCategory = async () => {
          let result = await fetch('https://www.eventneedz.com/api/events/category/'+ params.id);
          result=await result.json();
		  setName(data.name);
		  setCity(data.city);
		  setSeoName(data.seoName);
		  setSlotName(data.slotName);
          setData(result);
  
  
      }
      useEffect(() => {
		getCategory();
      });
	 async function editProduct(id){
			const formData= new FormData();
			formData.append('name', name);
			formData.append('city', city);
			formData.append('seoName', seoName);
			formData.append('slotName', slotName);

		let result =  fetch("'https://www.eventneedz.com/api/events/category/'+ params.id",{
		  method:'PUT',
		  body:formData,
		
		});
		alert("data update"+ id)
	  }

    return <>
          <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"><Link to="/Category"> Category </Link></li>
								<li className="breadcrumb-item"> Update Category  </li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
		<div className="col-lg-12 col-12">
        <div className="box">
						
				
							<div className="box-body">
								<h4 className="mt-0 mb-20">Update Category :</h4>
								<div className="form-group">
									<label> Name:</label>
									<input type="text" defaultValue={data.name} className='form-control' placeholder='Name' onChange={(e) =>setName(e.target.value)}/>
								</div>
								<div className="form-group">
									<label> City:</label>
									<input type="text" defaultValue={data.city} placeholder='City' onChange={(e) =>setCity(e.target.value)} className='form-control' />
								</div>
								<div className="form-group">
									<label> Seo Name:</label>
									<input type="text" defaultValue={data.seoName} onChange={(e) =>setSeoName(e.target.value)} placeholder='Seo Name' className='form-control' />
								</div>
								
								<div className="form-group">
									<label> Sloat Name:</label>
									<input type="text" defaultValue={data.slotNames} onChange={(e) =>setSlotName(e.target.value)}  placeholder='Sloat Name' className='form-control' />
								</div>
								
								
							</div>
							<div className="box-footer">
								<button className="btn  btn-primary" onClick={() =>editProduct(data.id)}>Update Category</button>
							</div>
							
						
					  </div>
				</div>
                </div>
                </section>
       
    </>
}
export default UpdateCategory;