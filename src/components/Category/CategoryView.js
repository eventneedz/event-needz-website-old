// // 
// import React, {useState, useEffect } from 'react';
// import { Link ,useParams} from 'react-router-dom';


// function CategoryView (props) {
//     const params = useParams();

//     const [data, setData] = useState([]);
//       const getUsers = async () => {
//           let result = await fetch('https://www.eventneedz.com/api/events/category/'+ params.id);
//           result=await result.json();
  
//           setData(result);
  
  
//       }
//       useEffect(() => {
//           getUsers();
//       });
//     return <>
   
//       <div className="content-header">
// 			<div className="d-flex align-items-center">
// 				<div className="mr-auto">
// 					<div className="d-inline-block align-items-center">
// 						<nav>
// 							<ol className="breadcrumb">
// 								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
// 								<li className="breadcrumb-item"> Category View </li>
// 							</ol>
// 						</nav>
// 					</div>
					
// 				</div>
				
// 			</div>
// 		</div>
//         <section className="content">
//           <div className="row">
//           <div className="col-12">
//               <div className="box">
//               <div className="box-body">
//     <div className="row">
//       <div className='col-md-12 mb-20'><img src={'https://www.eventneedz.com/'+data.campaignBanner} id="product-image" className="img-fluid" alt="" /></div>
//         <div className="col-md-5 col-sm-6">
//             <div className="box box-body b-1 text-center no-shadow">
//             <img src={'https://www.eventneedz.com/'+data.image} id="product-image" className="img-fluid" alt="" />
                
//             </div>
          
//         </div>
//         <div className="col-md-7 col-sm-6">
//             <h2 className="box-title mt-0">{data.name}</h2>
//             <div className="list-inline">
//                 <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
//                 <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
//                 <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
//                 <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
//                 <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
//             </div>
            
//             <p>{data.description} believable. but the majority have suffered alteration in some form, by injected humour</p>
           
//             <div className="gap-items" style={{marginTop:"10px"}}>
//                 <button className="btn btn-success"><i className="fa fa-shopping"></i> Buy Now!</button>
//                 <button className="btn btn-primary"><i className="fa fa-cart-plus"></i> Add To Cart</button>
           
//             </div>
            
//         </div>
//         <div className="col-lg-12 col-md-12 col-sm-12">
//             <h4 className="box-title mt-40">Key Highlights</h4>
//             <div className="table-responsive">
//                 {data.name}
                   

                    
//             </div>
//         </div>
//     </div>
// </div>
//               </div>
//             </div>
//             </div>
//         </section>
       
//     </>
// }
// export default CategoryView;
import React, {useState, useEffect } from 'react';
import { Link ,useParams} from 'react-router-dom';


function CategoryView (props) {
    const params = useParams();

    const [data, setData] = useState([]);
      const getUsers = async () => {
          let result = await fetch('https://www.eventneedz.com/api/events/category/'+ params.id);
          result=await result.json();
  
          setData(result);
  
  
      }
      useEffect(() => {
          getUsers();
      });
    return <>
   
      <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"> Category View </li>
							</ol>
						</nav>
					</div>
					
				</div>
				
			</div>
		</div>
        <section className="content">
          <div className="row">
          <div className="col-12">
              <div className="box">
              <div className="box-body">
    <div className="row">
      <div className='col-md-12 mb-20'><img src={'https://www.eventneedz.com/'+data.campaignBanner} id="product-image" className="img-fluid" alt="" /></div>
        <div className="col-md-5 col-sm-6">
            <div className="box box-body b-1 text-center no-shadow">
            <img src={'https://www.eventneedz.com/'+data.image} id="product-image" className="img-fluid" alt="" />
                
            </div>
          
        </div>
        <div className="col-md-7 col-sm-6">
            <h2 className="box-title mt-0">{data.name}</h2>
            <div className="list-inline">
                <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
                <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
                <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
                <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
                <Link to="/" className="text-warning"><i className="fa fa-star"></i></Link>
            </div>
            
            <p>{data.description} believable. but the majority have suffered alteration in some form, by injected humour</p>
           
            <div className="gap-items" style={{marginTop:"10px"}}>
                <button className="btn btn-success"><i className="fa fa-shopping"></i> Buy Now!</button>
                <button className="btn btn-primary"><i className="fa fa-cart-plus"></i> Add To Cart</button>
           
            </div>
            
        </div>
        <div className="col-lg-12 col-md-12 col-sm-12">
            <h4 className="box-title mt-40">Key Highlights</h4>
            <div className="table-responsive">
                {data.name}
                   

                    
            </div>
        </div>
    </div>
</div>
              </div>
            </div>
            </div>
        </section>
       
    </>
}
export default CategoryView;