import React, {useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {CatApi} from './CatApi'
import CatList from './CatList';
import Spinner from 'react-bootstrap/Spinner'
const Category = () => {
	const [users, setUsers] = useState([]);
	const getUsers = async () => {
		const response = await fetch('https://www.eventneedz.com/api/events/category');
		
		setUsers(await response.json());
		$("#lodderGet").css("display", "none");
		//console.log(data);

	}
	useEffect(() => {
		getUsers();
	},[]);
	function viewOpration(id){
			alert(id)
	}
    return (
        <>
		 <div className="content-header">
                <div className="d-flex align-items-center">
                    <div className="mr-auto">
                        <div className="d-inline-block align-items-center">
                            <nav>
                                 <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
                                    <li className="breadcrumb-item">Category </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <Link to="/AddCategory" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Add Category </Link>
                </div>
		</div>
		<section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">Category List <Spinner animation="grow"  variant="info" size="2px" style={{ display: "block" }} id="lodderGet" /> </h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">
					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th>ID</th>
						
						  <th>Name</th>
						  <th>PHOTO</th>
						  <th>Total Product</th>
						  <th>Status</th>
                          <th>ACTIONS</th>
						</tr>
						{
							users.map((curElem) =>{
						//console.log(curElem);
								return(
									<tr>
										<td>{curElem.id}</td>
										<td>{curElem.name}</td>
								{/*	<td>
										<ul>
										{curElem.combos.map(combos => (
											<li key={combos.name}>{combos.name}</li>
											))}
											</ul>
										</td>*/}
									
									<td><img src={'https://eventneedz.com'+curElem.image} width={'100px'} alt=''/></td>
									<td>{curElem.OGDescription}</td>
									<td>{curElem.status}</td>
									<td>
										<Link to={"/CategoryView/"+curElem.id} className='badge badge-pill badge-warning' ><i className='fa fa-eye'></i></Link>
										<Link to={"/UpdateCategory/"+curElem.id} className='badge badge-pill badge-warning' ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></Link>
										 <Link to={"/CategoryView/"+curElem.id} className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
									</tr>
								)

							})
						}
						
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
        
    </>

    )
}
export default Category;


// import React, {useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';
// import {CatApi} from './CatApi'
// import CatList from './CatList';
// const Category = () => {
// 	const [users, setUsers] = useState([]);
// 	const getUsers = async () => {
// 		const response = await fetch('https://www.eventneedz.com/api/events/category');
		
// 		setUsers(await response.json());
// 		//console.log(data);

// 	}
// 	useEffect(() => {
// 		getUsers();
// 	},[]);
//     return (
//         <>
// 		 <div className="content-header">
//                 <div className="d-flex align-items-center">
//                     <div className="mr-auto">
//                         <div className="d-inline-block align-items-center">
//                             <nav>
//                                  <ol className="breadcrumb">
//                                     <li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
//                                     <li className="breadcrumb-item">Category </li>
//                                 </ol>
//                             </nav>
//                         </div>
//                     </div>
//                     <Link to="/AddCategory" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Add Category </Link>
//                 </div>
// 		</div>
// 		<section className="content">
// 		  <div className="row">
//           <div className="col-12">
// 			  <div className="box">
// 				<div className="box-header with-border">
// 				  <h4 className="box-title">Category List</h4>
// 				  <div className="box-controls pull-right">
// 					<div className="lookup lookup-circle lookup-right">
// 					  <input type="text" name="s" />
// 					</div>
// 				  </div>
// 				</div>
// 				<div className="box-body no-padding">
// 					<div className="table-responsive">
// 					  <table className="table table-hover">
// 						<tbody><tr>
// 						  <th>ID</th>
						
// 						  <th>Name</th>
// 						  <th>PHOTO</th>
// 						  <th>Total Product</th>
// 						  <th>Status</th>
//                           <th>ACTIONS</th>
// 						</tr>
// 						{
// 							users.map((curElem) =>{
// 						console.log(curElem);
// 								return(
// 									<tr>
// 										<td>{curElem.id}</td>
// 										<td>{curElem.name}</td>
// 								{/*	<td>
// 										<ul>
// 										{curElem.combos.map(combos => (
// 											<li key={combos.name}>{combos.name}</li>
// 											))}
// 											</ul>
// 										</td>*/}
									
// 									<td><img src={'https://eventneedz.com'+curElem.image} width={'100px'} alt=''/></td>
// 									<td>{curElem.OGDescription}</td>
// 									<td>{curElem.status}</td>
// 									<td><Link to="/ProductView" className='badge badge-pill badge-warning' ><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
// 									</tr>
// 								)

// 							})
// 						}
						
// 					  </tbody></table>
// 					</div>
// 				</div>
// 			  </div>
// 			</div>
//             </div>
//         </section>
        
//     </>

//     )
// }
// export default Category;

