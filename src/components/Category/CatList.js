import React from 'react';
import { Link } from 'react-router-dom';
const CatList = ({id,Photo,Name,TotalProduct,Status}) => {
    
    return(
        <>
       <tr>
						  <td>{id}</td>
						  <td><img src={Photo} width="100px" alt=''/></td>
						  <td>{Name}</td>
						  <td><span className="text-danger">{TotalProduct}</span></td>
                          <td><span  className="badge badge-pill badge-danger">{Status}</span>
                     
                          </td>
						  <td><Link to="/CategoryView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link> <Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-pencil'></i></Link></td>
						</tr>
        </>

    )
}
export default CatList;