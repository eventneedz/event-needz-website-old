import React, {useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Spinner from 'react-bootstrap/Spinner'

const Product = () => {
	const [users, setUsers] = useState([]);
	const getUsers = async () => {
		const response = await fetch('https://eventneedz.com/api/products');
		
		setUsers(await response.json());
		$("#lodderGet").css("display", "none");
		//console.log(data);

	}
	useEffect(() => {
		getUsers();
	},[]);
	
    return <>
        
		<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item">Product </li>
							</ol>
						</nav>
					</div>
				</div>
				<Link to="/AddProduct" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Add Product </Link>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">Product List <Spinner animation="grow"  variant="info" size="2px" style={{ display: "block" }} id="lodderGet" /></h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">
					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th width="100px">ID</th>
						  <th>Name </th>
						  <th  width="100px">PHOTO</th>
						  <th width="200px">DISCRIPTION</th>
						  <th>CREATED AT</th>
                          <th>ACTIONS</th>
						</tr>

						{
							users.map((curElem) =>{
						console.log(curElem);
								return(
									<tr>
										<td>{curElem.id}</td>
										<td>{curElem.name}</td>
								{/*	<td>
										<ul>
										{curElem.combos.map(combos => (
											<li key={combos.name}>{combos.name}</li>
											))}
											</ul>
										</td>*/}
									
									<td><img src={'https://eventneedz.com'+curElem.image} width={'100px'} alt=''/></td>
									<td>{curElem.description}</td>
									<td>{curElem.lastModifiedDate}</td>
									<td><Link to={"/UpdateProduct/"+curElem.id} className='badge badge-pill badge-warning' ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></Link></td>
									<td><Link to={"/ProductView/"+curElem.id} className='badge badge-pill badge-warning'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link></td>
									</tr>
								)

							})
						}
						
						
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
		
		
    </>
}
export default Product;