import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
class ProductView extends React.Component {
  constructor(probs) {
    super(probs);
    this.state = {
      arr: [],
    };
  }
  componentDidMount() {
    axios.get('https://eventneedz.com/api/products').then(
      (posRes) => {
        this.setState({ arr: posRes.data });
      },
      (errRes) => {
        console.log(errRes);
      }
    );
  }
  render() {
    return (
      <>
	  <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item">Product View</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">Product View</h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">
					<div className="table-responsive">
					  <table className="table no-border no-padding">
						<tbody>
						
						{this.state.arr.map((curElem) => {
              let a ='cb12a0ae-1c07-4432-8994-35a22e6a6133';
							return (
							<tr key={curElem.id}>
            	
								{curElem.combos && (
								<td><table className='table'>
								
												
									{curElem.combos.map((child, subindex) => (
									
											<tr>
												
											<td key={subindex}>{child.name}</td>
											
											<td>{curElem.name}</td>

											</tr>
										
									))}
								</table></td>
								)}
							</tr>
							
							);
						})}
							
						
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
		
        
      </>
    );
  }
}

export default ProductView;
