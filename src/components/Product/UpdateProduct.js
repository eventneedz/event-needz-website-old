import React, {useState, useEffect } from 'react';
import { Link ,useParams} from 'react-router-dom';
function UpdateCategory (props) {
	const params = useParams();
	const [data, setData] = useState([]);
	const [name,setName]=useState("");
 	const [quantity,setquantity]=useState("");
  const [description,setDescription]=useState("");
  const [seoName,setSeoName]=useState("");
  const [image,setImage]=useState("");
  const [campaignBanner,setCampaignBanner]=useState("");
  const [mobiledescription,setMobiledescription]=useState("");
  const [metaDescription,setMetaDescription]=useState("");

   
      const getProduct = async () => {
          let result = await fetch('https://www.eventneedz.com/api/product/'+ params.id);
          result=await result.json();
		  setName(data.name);
		  setquantity(data.quantity);
		  setDescription(data.description);
		  setSeoName(data.seoName);
          setImage(data.image);
          setCampaignBanner(data.campaignBanner);
          setMobiledescription(data.mobiledescription);
          setMetaDescription(data.metaDescription);
          setData(result);
  
  
      }
      useEffect(() => {
		getProduct();
      });
	 async function editProduct(id){
			const formData= new FormData();
			formData.append('name', name);
			formData.append('quantity', quantity);
			formData.append('description', description);
			formData.append('seoName', seoName);
            formData.append('image', image);
            formData.append('campaignBanner', campaignBanner);
            formData.append('mobiledescription', mobiledescription);
            formData.append('metaDescription', metaDescription);

		let result =  fetch('https://www.eventneedz.com/api/events/product/'+ params.id,{
		  method:'PUT',
		  body:formData,
		
		});
		alert("data update"+ id)
	  }

    return <>
          <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"><Link to="/Product"> Product </Link></li>
								<li className="breadcrumb-item"> Update Product  </li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
		<div className="col-lg-12 col-12">
        <div className="box">
						
				
							<div className="box-body">
								<h4 className="mt-0 mb-20">Update Product :</h4>
								<div className="form-group">
									<label> Product Name:</label>
									<input type="text" defaultValue={data.name} className='form-control' placeholder='Product Name' onChange={(e) =>setName(e.target.value)}/>
                                    
								</div>
                                <div className="form-group">
									<label> quantity:</label>
									<input type="text" defaultValue={data.quantity} onChange={(e) =>setquantity(e.target.value)} placeholder='quantity' className='form-control' />
								</div>
								<div className="form-group">
									<label> Description:</label>
									<input type="text" defaultValue={data.description} placeholder='description' onChange={(e) =>setDescription(e.target.value)} className='form-control' />
								</div>
							
								<div className="form-group">
									<label> seoName:</label>
									<input type="text" defaultValue={data.seoName} onChange={(e) =>setSeoName(e.target.value)}  placeholder='SeoName' className='form-control' />
								</div>
                                <div className="form-group">
									<label> image:</label>
									<input type="file" defaultValue={data.image} onChange={(e) =>setImage(e.target.value)}  placeholder='Image' className='form-control' />
								</div>
                                <div className="form-group">
									<label> campaignBanner:</label>
									<input type="text" defaultValue={data.campaignBanner} onChange={(e) =>setCampaignBanner(e.target.value)}  placeholder='CampaignBanner' className='form-control' />
								</div>
                                <div className="form-group">
									<label> mobiledescription:</label>
									<input type="text" defaultValue={data.mobiledescription} onChange={(e) =>setMobiledescription(e.target.value)}  placeholder='Mobiledescription' className='form-control' />
								</div>
                                <div className="form-group">
									<label> Description:</label>
									<input type="text" defaultValue={data.metaDescription} onChange={(e) =>setmetaDescription(e.target.value)}  placeholder='MetaDescription' className='form-control' />
								</div>
								
								
							</div>
							<div className="box-footer">
								<button className="btn  btn-primary" onClick={() =>editProduct(data.id)}>Update Product</button>
							</div>
							
						
					  </div>
				</div>
                </div>
                </section>
       
    </>
}
export default UpdateCategory;