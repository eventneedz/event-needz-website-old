// import React, { Component } from 'react';

// import alertify from 'alertifyjs';
// import 'alertifyjs/build/css/alertify.css';

// class AddProduct extends Component {
// 	constructor() {
// 		super();
// 		this.state = {
// 			name: null,
// 			quantity: null,
// 			description: null,
// 			seoName: null,
// 			image: null,
// 			campaignBanner: null,
// 			metaDescription: null,
// 			nameError:"",
// 			quantity:""
// 		};
// 	}
// 	valid()
// 	{
		
// 		if (!this.state.name.includes("")) 
// 		{
// 			this.setState({nameError:"invalid name"})
// 		}
// 	}

	

// 	create() {
// 		if (this.valid()) {
// 			alert("form sumited")
// 		}





// 		let url = 'https://www.eventneedz.com/api/product';
// 		fetch(url, {
// 			method: 'Post',
// 			headers: {
// 				'Accept': 'application/json',
// 				'Content-Type': 'application/json',
// 			},
// 			body: JSON.stringify(this.state)
// 		}).then((result => {
// 			result.json().then((resp) => {
// 				console.warn("test", resp.id)
// 				if (resp.id) {
// 					alert()
// 					alertify.success('Product added successfully Please wait for verification');
// 				}

// 			})
// 		}))
// 	}
// 	render() {
// 		return (

// 			<>
// 			<div className="form-group">
// 				<input type="text" className="form-control" placeholder="Product Name" onChange={(event) => { this.setState({ name: event.target.value }) }} required />
// 				<div style={{color:"red"}}>{this.state.nameError}</div>
// 				</div>
// 				<input type="text" name='quantity' className="form-control" placeholder="quantity" required onChange={(event) => { this.setState({ quantity: event.target.value }) }} />
// 				<input type="text" name='description' className="form-control" placeholder="description" required onChange={(event) => { this.setState({ description: event.target.value }) }} />
// 				<input type="text" name='seoName' className="form-control" placeholder="seoName" required onChange={(event) => { this.setState({ seoName: event.target.value }) }} />
// 				<input type="file" name='image' className="form-control" placeholder="image" required onChange={(event) => { this.setState({ image: event.target.value }) }} />
// 				<input type="text" name='campaignBanner' className="form-control" placeholder="campaignBanner" required onChange={(event) => { this.setState({ campaignBanner: event.target.value }) }} />
// 				<input type="text" name='mobiledescription' className="form-control" placeholder="mobiledescription" required onChange={(event) => { this.setState({ mobiledescription: event.target.value }) }} />
// 				<input type="text" name='metaDescription' className="form-control" placeholder="metaDescription" required onChange={(event) => { this.setState({ metaDescription: event.target.value }) }} />

// 				<button onClick={() => { this.create() }}>Add</button>
// 			</>
// 		);
// 	}
// }



// export default AddProduct;
import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import alertify from 'alertifyjs';
import 'alertifyjs/build/css/alertify.css';



function AddProduct() {
	
	const [name,setName]=useState("");
 	const [quantity,setQuantity]=useState("");
  const [description,setDescription]=useState("");
  const [seoName,setSeoName]=useState("");
  const [image,setImage]=useState("");
  const [campaignBanner,setCampaignBanner]=useState("");
  const [mobiledescription,setMobiledescription]=useState("");
  const [metaDescription,setMetaDescription]=useState("");

	async function addinfo() {
		let item = { name, quantity, seoName ,description, image,campaignBanner,mobiledescription,metaDescription }
		console.warn(item);
		let result = await fetch("https://www.eventneedz.com/api/events/category", {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json"
			},
			body: JSON.stringify(item)
		})
		result = await result.json();
		console.log("test", result.id)
		if (result.id) {
			
			alertify.success('Your Product Successfull Added Please wait for verification ');
		}
		console.warn(result);

		setName("");
		setQuantity("");
		setDescription("");
		setSeoName("");
		setImage("");
		setCampaignBanner("");
		setMobiledescription("");
		setMetaDescription("");


	}
	return <>
		<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
							<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"><Link to="/product"> Product </Link></li>
								<li className="breadcrumb-item">Add Product</li>
							</ol>
						</nav>
					</div>
				</div>

			</div>
		</div>
		<section className="content">
			<div className="row">
				<div className="col-lg-12 col-12">
					<div className="box">
						<div className="box-body">
							<h4 className="mt-0 mb-20">Add Product :</h4>
							<div className="form-group">
								<label> Product Name:</label>
								<input type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder=' ProductName' className='form-control' required />
							</div>
						
							<div className="form-group">
								<label> quantity:</label>
								<input type="text" value={quantity} onChange={(e) => setQuantity(e.target.value)} placeholder='quantity' className='form-control' required />
							</div>
							<div className="form-group">
								<label> Description:</label>
								<input type="text" value={description} onChange={(e) => setDescription(e.target.value)} placeholder='Description' className='form-control' required />
							</div>
							<div className="form-group">
								<label> SeoName:</label>
								<input type="text" value={seoName} onChange={(e) => setSeoName(e.target.value)} placeholder='seoName' className='form-control' required />
							</div>
							<div className="form-group">
								<label> Image:</label>
								<input type="file" value={image} onChange={(e) => setImage(e.target.value)} placeholder='Seo Name' className='form-control' required />
							</div>
							<div className="form-group">
								<label> CampaignBanner:</label>
								<input type="text" value={campaignBanner} onChange={(e) => setCampaignBanner(e.target.value)} placeholder='CampaignBanner' className='form-control' required />
							</div>
							<div className="form-group">
								<label> Mobiledescription:</label>
								<input type="text" value={mobiledescription} onChange={(e) => setMobiledescription(e.target.value)} placeholder='Mobiledescription' className='form-control' required />
							</div>
							<div className="form-group">
								<label> MetaDescription:</label>
								<input type="text" value={metaDescription} onChange={(e) => setMetaDescription(e.target.value)} placeholder='metaDescription' className='form-control' required />
							</div>
							
						</div>
						<div className="box-footer">
							<button type="submit" className="btn btn-warning" style={{ marginRight: "10px" }}>Reset</button>
							<button className="btn  btn-primary" onClick={addinfo}>Add Product-</button>
						</div>

					</div>
				</div>
			</div>
		</section>
	</>
}
export default AddProduct;
