import React from 'react';
import { Link } from 'react-router-dom';

 
function VerifyProduct () {
    return <>
        	<div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item">Verify Product  </li>
							</ol>
						</nav>
					</div>
				</div>
				<Link to="/AddVerify" className='btn btn-primary'> <i class="fa fa-plus-circle"></i> Add Promocode </Link>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
          <div className="col-12">
			  <div className="box">
				<div className="box-header with-border">
				  <h4 className="box-title">Verify Product </h4>
				  <div className="box-controls pull-right">
					<div className="lookup lookup-circle lookup-right">
					  <input type="text" name="s" />
					</div>
				  </div>
				</div>
				<div className="box-body no-padding">
					<div className="table-responsive">
					  <table className="table table-hover">
						<tbody><tr>
						  <th>ID</th>
						  <th>Promo Code</th>
						  <th>Message</th>
						  <th>Start Date</th>
						  <th>End Date</th>
                          <th>Discount</th>
						  <th>Discount Type</th>
						  <th>Status</th>
                          <th>ACTIONS</th>
						</tr>
						<tr>
						  <td>1</td>
						  <td> #2345</td>
						  <td>In many ways, the blue verification  </td>
                          <td>02/03/2021</td>
						  <td>02/03/2021</td>
                          <td>0</td><td>percentage</td>
                          <td><span className="badge badge-pill badge-danger">Cancle</span></td>
						  <td><Link to="/ProductView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link> <Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-pencil'></i></Link></td>
						</tr>
						<tr>
						  <td>2</td>
						  <td> #2345</td>
						  <td>In many ways, the blue verification  </td>
                          <td>02/03/2021</td>
						  <td>02/03/2021</td>
                          <td>0</td><td>percentage</td>
                          <td><span className="badge badge-pill badge-primary">Active</span></td>
						  <td><Link to="/ProductView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link> <Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-pencil'></i></Link></td>
						</tr>
                        <tr>
						  <td>3</td>
						  <td> #2345</td>
						  <td>In many ways, the blue verification  </td>
                          <td>02/03/2021</td>
						  <td>02/03/2021</td>
                          <td>0</td><td>percentage</td>
                          <td><span className="badge badge-pill badge-danger">Cancle</span></td>
						  <td><Link to="/ProductView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link> <Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-pencil'></i></Link></td>
						</tr>
						<tr>
						  <td>4</td>
						  <td> #2345</td>
						  <td>In many ways, the blue verification  </td>
                          <td>02/03/2021</td>
						  <td>02/03/2021</td>
                          <td>0</td><td>percentage</td>
                          <td><span className="badge badge-pill badge-primary">Active</span></td>
						  <td><Link to="/ProductView" className='badge badge-pill badge-primary'><i className='fa fa-eye'></i></Link> <Link to="/" className='badge badge-pill badge-danger'><i className='fa fa-trash'></i></Link> <Link to="/" className='badge badge-pill badge-warning'><i className='fa fa-pencil'></i></Link></td>
						</tr>
					  </tbody></table>
					</div>
				</div>
			  </div>
			</div>
            </div>
        </section>
       
    </>
}
export default VerifyProduct;