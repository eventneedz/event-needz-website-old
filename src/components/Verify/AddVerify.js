import React from 'react';
import { Link } from 'react-router-dom';
function AddVerify () {
    return <>
        <div className="content-header">
			<div className="d-flex align-items-center">
				<div className="mr-auto">
					<div className="d-inline-block align-items-center">
						<nav>
						<ol className="breadcrumb">
								<li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i> Dashboard</Link></li>
								<li className="breadcrumb-item"> Add Verify  </li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
        <section className="content">
		  <div className="row">
		<div className="col-lg-12 col-12">
        <div className="box">
						
						<form>
							<div className="box-body">
								<h4 className="mt-0 mb-20"> Add Promo Code</h4>
                                <div className='row'>
                                    <div className='col-md-6'>
                                        <div className="form-group">
                                            <label>Promo Code:</label>
                                            <input type="email" className="form-control" placeholder="Enter Promo Code"/>
                                        </div>
                                    </div>
                                    <div className='col-md-6'>
                                        <div className="form-group">
                                            <label>Message :</label>
                                            <input type="email" className="form-control" placeholder="Enter Message "/>
                                        </div>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col-md-6'>
                                        
                                        <div className="form-group ">
                                            <label>Start Date :</label>
                                            <input type="date" className="form-control" placeholder="Enter Start Date" id="datepicker"/>
                                        </div>
                                    </div>
                                    <div className='col-md-6'>
                                        <div className="form-group">
                                            <label>End Date :</label>
                                            <input type="date" className="form-control" placeholder="Enter Last Date "/>
                                        </div>
                                    </div>
                                </div>
								
								<div className='row'>
                                    <div className='col-md-6'>
										<div className="form-group">
											<label>No Of User's </label>
											<input type="number" className="form-control" placeholder="No Of User's "/>
										</div>
									</div>
									<div className='col-md-6'>
										<div className="form-group">
											<label>No Of User's </label>
											<input type="number" className="form-control" placeholder="No Of User's "/>
										</div>
									</div>
									<div className='col-md-6'>
										<div className="form-group">
											<label>Discount</label>
											<input type="number" className="form-control" placeholder="Discount "/>
										</div>
									</div>
									<div className='col-md-6'>
										<div className="form-group">
											<label>Discount Type </label>
											<select className="browser-default custom-select">
												<option>Choose your option</option>
												<option value="1">Option 1</option>
												<option value="2">Option 2</option>
												<option value="3">Option 3</option>
											</select>
										</div>
									</div>
									<div className='col-md-6'>
										<div className="form-group">
											<label>Max Discount Amount</label>
											<input type="number" className="form-control" placeholder="Discount "/>
										</div>
									</div>
									<div className='col-md-6'>
										<div className="form-group">
											<label>Repeat Usage</label>
											<select className="browser-default custom-select">
												<option>Choose your option</option>
												<option value="1">Option 1</option>
												<option value="2">Option 2</option>
												<option value="3">Option 3</option>
											</select>
										</div>
									</div>
									<div className='col-md-6'>
										<div className="form-group">
											<label>Status</label>
											<select className="browser-default custom-select">
												<option>Choose your option</option>
												<option value="1">Option 1</option>
												<option value="2">Option 2</option>
												<option value="3">Option 3</option>
											</select>
										</div>
									</div>
								</div>
								
								
								
							</div>
							<div className="box-footer">
								<button type="submit" className="btn btn-danger">Cancel</button>
								<button type="submit" className="btn  btn-success pull-right">Submit</button>
							</div>
						</form>
					  </div>
				</div>
                </div>
                </section>
		
    </>
}
export default AddVerify;